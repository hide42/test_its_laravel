<?php

namespace App\Http\Controllers;

use App\Http\Resources\modResource;
use App\Mod;
use Illuminate\Http\Request;

class modsController extends Controller
{
    public $validate_rules = [
        'name' => 'required|unique:mods|max:255',
        'surname' => 'required|max:255',
        'middle_name' => 'required|max:255',
        'job' => 'required|max:255',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return modResource::Collection(Mod::all());
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate($this->validate_rules);
        try{
            $mod = Mod::create([
                'name' => $request->name,
                'surname' => $request->surname,
                'middle_name' => $request->middle_name,
                'job' => $request->job,
            ]);
        }catch (\Exception $e){
        }

        return new modResource($mod);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mod $mod)
    {
        return new modResource($mod);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mod $mod)
    {
        $own_validate_rules = $this->validate_rules;
        $own_validate_rules['name'] = 'required|max:255|unique:mods,id,'.$request->id;
        $validatedData = $request->validate($own_validate_rules);
        try {
            $mod->update($request->all());
        } catch (\Exception $e) {
            return "gabella";
        }
        return new modResource($mod);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mod $mod)
    {
        try {
            $mod->delete();
        } catch (\Exception $e) {
        }
        return response()->json("completed",204);
        //
    }
}
